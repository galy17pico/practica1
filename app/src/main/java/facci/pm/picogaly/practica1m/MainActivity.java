package facci.pm.picogaly.practica1m;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button buttonLogin, buttonGuardar, buttonBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonGuardar = findViewById(R.id.buttonGuardar);
        buttonBuscar = findViewById(R.id.buttonBuscar);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
            });
             buttonGuardar.setOnClickListener(new View.OnClickListener() {
                 @Override
                    public void onClick(View view) {
                     Intent intent = new Intent(
                             MainActivity.this, GuardarActivity.class);
                     startActivity(intent);
                                               }
             });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        MainActivity.this, BuscarActivity.class);
                startActivity(intent);
            }
            });
    }
}


